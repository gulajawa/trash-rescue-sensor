#include <NewPing.h>
#include <SoftwareSerial.h>

#define TRIGGER_PIN1  6
#define ECHO_PIN1     7

#define TRIGGER_PIN2  8
#define ECHO_PIN2     9

#define MAX_DISTANCE 250

const char* host = "api.arizalsaputro.net";
String lat="-6.974001";
String lng="107.6281593";
long waktu, jarak, waktu2, jarak2;

SoftwareSerial sim900(2, 3); // RX, TX

NewPing sonar1(TRIGGER_PIN1, ECHO_PIN1, MAX_DISTANCE); 
NewPing sonar2(TRIGGER_PIN2, ECHO_PIN2, MAX_DISTANCE); 

void setup(){
  pinMode(0,INPUT_PULLUP);
  sim900.begin(9600);
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  status_gsm(); //prosedur memeriksa koneksi sim900 dengan arduino
  setting_kartusim(); //prosedur pengaturan GPRS sim900
}

void loop(){
  delay(100);
  sensor1();
  delay(1000);
  sensor2();
  //delay(1000);
  koneksi();
  delay(5000);
}

void sensor1(){
  Serial.print("Jarak 1 : ");
  jarak = sonar1.ping_cm();
  Serial.print(jarak);
  Serial.println(" cm");
}

void sensor2(){
  Serial.print("Jarak 2 : ");
  jarak2 = sonar2.ping_cm();
  Serial.print(jarak2);
  Serial.println(" cm");
}

long microsecondsToCentimeters(long microseconds){
  return microseconds/29/2;
}

void status_gsm(){
  sim900.println(F("AT")); //AT command untuk memastikan koneksi dengan Arduino
  if(sim900.find("OK")){
    Serial.println(F("Koneksi dengan Arduino BERHASIL"));
  }else{
    Serial.println(F("Koneksi dengan Arduino GAGAL"));
    reset_gsm(); //prosedur power up sim900 (melakukan hard reset sim900)
    status_gsm();
  }
  delay(100);
}

void reset_gsm(){
  pinMode(9, OUTPUT); 
  digitalWrite(9,LOW);
  delay(100);
  digitalWrite(9,HIGH);
  delay(200);
  digitalWrite(9,LOW);
  delay(300);
}

void setting_kartusim(){
  sim900.println(F("AT+CREG=1")); //mengaktifkan registrasi jaringan
  delay(100);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CGATT=1")); //masuk ke gprs servis
  delay(100);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CIPSHUT")); //menonaktifkan gprs
  delay(100);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CIPMUX=0"));//mengaktifkan single IP koneksi
  delay(100);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CSTT=\"internet""\"")); //setting APN kartu sim
  delay(500);
  sim900.println(F("AT+CSTT?")); //memastikan setting APN benar
  delay(5000);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CIICR")); //memulai koneksi GPRS
  delay(5000);
  Serial.println(sim900.readString());
  sim900.println(F("AT+CIFSR")); //request IP
  delay(5000);
  Serial.println(sim900.readString());
  pinMode(LED_BUILTIN, HIGH);
}

void koneksi(){
  if(jarak==0 || jarak2==0){
    return;
  }
  if(!(jarak>=1 && jarak<=35 && jarak2>=1 && jarak2<=35)){
    return;
  }
  sim900.println(F("AT+CIPSTART=\"TCP\",\"www.api.arizalsaputro.net""\",80")); //memulai koneksi dengan server
  Serial.println(sim900.readString());   
  digitalWrite(LED_BUILTIN, HIGH); 
  if(sim900.find("OK")){
    Serial.println(F("proses data")); 
  }
  String url = "/sensor/single/update?api_key=oZKyqXKcubkGEChGZB63NhYEpSSmxv";
  String link = "GET " + url + "&jarak_sensor_1="+ jarak + "&jarak_sensor_2=" + jarak2+ "&lat="+ lat + "&lng="+ lng +" HTTP/1.1\r\nHost: " + host + "\r\n\r\n"; //tautan dengan metode GET
  sim900.print(F("AT+CIPSEND=")); //mengirim request data
  pinMode(LED_BUILTIN, HIGH);
  sim900.println(link.length()); //mengirim panjang tautan request data
  if(sim900.find(">")){
    sim900.print(link); //mengirim tautan ke server
    if(sim900.find("SEND OK")){
      pinMode(LED_BUILTIN, HIGH);
      delay(100);
      while (sim900.available()){
        Serial.println(sim900.readString()); //feedback data (sesuai dengan keluaran di browser)
      }
      sim900.println(F("AT+CIPCLOSE")); //stop koneksi
    }else{
      koneksi();
    }
  }
}
